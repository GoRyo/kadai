import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class kadai {

    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JTextPane total;


    int Total = 0;
    void order(String food, int price){

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food +" " + price + "yen\n");
            Total += price;
            total.setText("Total   " + Total + "  yen" );
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + " ! It will be served as soon as possible."
            );

        }

    }



    public kadai() {
        total.setText("Total   " + Total + "  yen" );
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger Steak",
                        100);

            }
        });
        button1.setIcon(new ImageIcon(
                this.getClass().getResource("hamburger.jpg")
        ));



        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pasta",
                        150);

            }
        });
        button2.setIcon(new ImageIcon(
                this.getClass().getResource("pasta.jpg")
        ));




        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak",
                        200);

            }
        });
        button3.setIcon(new ImageIcon(
                this.getClass().getResource("steak.jpg")
        ));




        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",
                        250);

            }
        });
        button4.setIcon(new ImageIcon(
                this.getClass().getResource("sushi.jpg")
        ));





        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",
                        300);

            }
        });
        button5.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));




        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",
                        350);

            }
        });
        button6.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));



        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. " + "Total is " + Total + " yen."
                    );
                }
                orderedItemsList.setText("");
                Total = 0;
                total.setText("Total   " + Total + "  yen" );
            }

        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
